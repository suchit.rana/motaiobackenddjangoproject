from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from finance.models import Finance, UserDetails
from finance.serializers import (FinanceCreateSerializer, FinanceSerializer,
                                 UserDetailsSerializer)


class UserDetailsCreateAPI(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = UserDetails.objects.all()
    serializer_class = UserDetailsSerializer


class FinanceCreateAPI(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Finance.objects.all()
    serializer_class = FinanceCreateSerializer


class FinanceListAPI(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Finance.objects.all()
    serializer_class = FinanceSerializer


class GetSingleFinanceAPI(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Finance.objects.all()
    serializer_class = FinanceSerializer
    lookup_field = "finance_id"


class FinanceUpdateAPI(generics.RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Finance.objects.all()
    serializer_class = FinanceCreateSerializer
    lookup_field = "finance_id"


class FinanceDeleteAPI(generics.DestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Finance.objects.all()
    serializer_class = FinanceSerializer
    lookup_field = "finance_id"

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(data={"message": "Deleted"}, status=status.HTTP_204_NO_CONTENT)
