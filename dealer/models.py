from djongo import models

from authentication.models import User
from organisation.models import Organisation
from utility.utils import generate_dealer_id


class Dealer(models.Model):

    dealer_id = models.CharField(
        primary_key=True, unique=True, max_length=30, default=generate_dealer_id)
    dealer_name = models.CharField(max_length=500)
    dealer_address = models.TextField()
    primary_employee = models.ForeignKey(to=User, on_delete=models.CASCADE)
    organisation = models.ForeignKey(to=Organisation, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Dealer"
        verbose_name_plural = "Dealers"
        ordering = ('dealer_name',)

    def __str__(self):
        return self.dealer_name
