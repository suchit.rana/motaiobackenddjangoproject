from rest_framework import serializers

from vehicle.models import VechicleExpense, VechicleImage, VechicleInfo


class VechicleExpenseSerializer(serializers.ModelSerializer):
    class Meta:
        model = VechicleExpense
        fields = '__all__'


class VechicleImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = VechicleImage
        fields = '__all__'


class VechicleInfoCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = VechicleInfo
        fields = '__all__'


class VechicleInfoSerializer(serializers.ModelSerializer):

    vehicle_expenses = VechicleExpenseSerializer(many=True)
    vehicle_images = VechicleImageSerializer(many=True)

    class Meta:
        model = VechicleInfo
        fields = '__all__'
