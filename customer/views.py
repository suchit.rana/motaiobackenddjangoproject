from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from customer.models import CustomerAddress, CustomerInfo
from customer.serializers import (CustomerAddressCreateSerializer,
                                  CustomerAddressSerializer,
                                  CustomerInfoCreateSerializer,
                                  CustomerInfoSerializer)


class CustomerInfoCreateAPI(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = CustomerInfo.objects.all()
    serializer_class = CustomerInfoCreateSerializer

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)


class GetSingleCustomerAPI(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = CustomerInfo.objects.all()
    serializer_class = CustomerInfoSerializer
    lookup_field = "customer_id"


class CustomerUpdateAPI(generics.RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = CustomerInfo.objects.all()
    serializer_class = CustomerInfoCreateSerializer
    lookup_field = "customer_id"


class CustomerListAPI(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = CustomerInfo.objects.all()
    serializer_class = CustomerInfoSerializer

    def get_queryset(self):
        return CustomerInfo.objects.filter(
            created_by=self.request.user
        )


class CustomerInfoDeleteAPI(generics.DestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = CustomerInfo.objects.all()
    serializer_class = CustomerInfoSerializer
    lookup_field = "customer_id"

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(data={"message": "Deleted"}, status=status.HTTP_204_NO_CONTENT)


class CustomerAddressCreateAPI(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = CustomerAddress.objects.all()
    serializer_class = CustomerAddressCreateSerializer


class CustomerAddressListAPI(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = CustomerAddress.objects.all()
    serializer_class = CustomerAddressSerializer

    def get_queryset(self):
        customer_id = self.kwargs['customer_id']
        return CustomerAddress.objects.filter(
            customer__customer_id=customer_id
        )


class CustomerAddressDeleteAPI(generics.DestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = CustomerAddress.objects.all()
    serializer_class = CustomerAddressSerializer
    lookup_field = "address_id"

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(data={"message": "Deleted"}, status=status.HTTP_204_NO_CONTENT)


class CustomerAddressUpdateAPI(generics.RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = CustomerAddress.objects.all()
    serializer_class = CustomerAddressCreateSerializer
    lookup_field = "address_id"
