from django.urls import path

from vehicle.views import (GetSingleVechicleInfoAPI, VechicleInfoCreateAPI,
                           VechicleInfoDeleteAPI, VechicleInfoListAPI,
                           VechicleInfoUpdateAPI)

urlpatterns = [
    path('create', VechicleInfoCreateAPI.as_view(), name="create_vehicle_info"),
    path('list', VechicleInfoListAPI.as_view(), name="list_vehicle_info"),
    path('update/<str:vehicle_id>', VechicleInfoUpdateAPI.as_view(),
         name="update_vehicle_info"),
    path('delete/<str:vehicle_id>', VechicleInfoDeleteAPI.as_view(),
         name="delete_vehicle_info"),
    path('<str:vehicle_id>', GetSingleVechicleInfoAPI.as_view(),
         name="get_vehicle_info"),
]
