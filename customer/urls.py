from django.urls import path

from customer.views import (CustomerAddressCreateAPI, CustomerAddressDeleteAPI,
                            CustomerAddressListAPI, CustomerAddressUpdateAPI,
                            CustomerInfoCreateAPI, CustomerInfoDeleteAPI,
                            CustomerListAPI, CustomerUpdateAPI,
                            GetSingleCustomerAPI)

urlpatterns = [
    path('info/create', CustomerInfoCreateAPI.as_view(), name="create_customer"),
    path('info/update/<str:customer_id>',
         CustomerUpdateAPI.as_view(), name="update_customer"),
    path('info/delete/<str:customer_id>',
         CustomerInfoDeleteAPI.as_view(), name="delete_customer"),
    path('list', CustomerListAPI.as_view(), name="list_customer"),
    path('address/create', CustomerAddressCreateAPI.as_view(),
         name="create_customer_address"),
    path('address/list/<str:customer_id>',
         CustomerAddressListAPI.as_view(), name="list_customer_address"),
    path('address/delete/<str:address_id>',
         CustomerAddressDeleteAPI.as_view(), name="delete_customer_address"),
    path('address/update/<str:address_id>',
         CustomerAddressUpdateAPI.as_view(), name="update_customer_address"),
    path('<str:customer_id>', GetSingleCustomerAPI.as_view(), name="get_customer")
]
