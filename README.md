# motaiobackend

Documentation Url - [{APP_BASE_URL}/swagger]({APP_BASE_URL}/swagger)

### Runing this application
1. Install Python
2. Install Poetry
```bash
pip install poetry
```
3. Change directory to app directory and  install dependencies
```bash
cd motaiobackend && poetry install
```
4. Create a .env using .env.example as a sample. For production environment, change DJANGO_SETTINGS_MODULE to `motaiobackend.settings.prod`
5. DB and migrations
```bash
poetry run python manage.py makemigrations
poetry run python manage.py migrate
```
6. Run the application.
```bash
poetry run python manage.py runserver
```


