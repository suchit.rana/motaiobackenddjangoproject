from django.contrib import admin

from dealer.models import Dealer


class DealerAdmin(admin.ModelAdmin):
    list_display = ('dealer_id', 'dealer_name', 'dealer_address',
                    'primary_employee', 'organisation', 'created_at', 'updated_at')


admin.site.register(Dealer, DealerAdmin)
