import json

from django.test import Client, TestCase
from django.urls import reverse
from rest_framework import status

from authentication.models import User
from utility.utils import generate_user_id, generate_vehicle_id
from vehicle.models import VechicleInfo
from vehicle.serializers import VechicleInfoSerializer


class CustomerTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.user = User.objects.create(
            user_id=generate_user_id(),
            username="johndoe@gmail.com",
            email="johndoe@gmail.com",
            first_name="John",
            last_name="Doe",
            business_name="XYZ and sons"
        )
        self.user.set_password("john$@Doe")
        self.user.save()
        #
        self.client.login(username='johndoe@gmail.com', password='john$@Doe')
        self.vehicle_info = VechicleInfo.objects.create(
            vehicle_id=generate_vehicle_id(),
            stock_code="123",
            manufacturer="Toyota",
            model="Camry",
            year="2022"
        )

    def test_create_vehicle(self):
        response = self.client.post(
            reverse('create_vehicle_info'),
            data=json.dumps({
                "stock_code": "127839",
                "manufacturer": "Hyndia",
                "model": "Elantra",
                "year": "2021",
                "engine_no": "1232222399fJH",
                "vehicle_expenses": [],
                "vehicle_images": []
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_vehicle(self):
        response = self.client.put(
            reverse('update_vehicle_info', kwargs={
                    'vehicle_id': self.vehicle_info.vehicle_id}),
            data=json.dumps({
                "stock_code": "123",
                "manufacturer": "Toyota",
                "model": "Camry",
                "year": "2020",
                "engine_no": "PK1232222399fJH",
                "vehicle_expenses": [],
                "vehicle_images": []
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        json_data = response.json()
        self.assertIn('year', json_data)
        self.assertEquals(json_data['year'], '2020')
        self.assertIn('engine_no', json_data)
        self.assertEquals(json_data['engine_no'], 'PK1232222399fJH')

    def test_get_vehicle(self):
        response = self.client.get(
            reverse('get_vehicle_info', kwargs={
                    'vehicle_id': self.vehicle_info.vehicle_id}),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.vehicle_info.refresh_from_db()
        vehicle_info = VechicleInfoSerializer(
            self.vehicle_info, many=False)
        self.assertDictEqual(response.json(), vehicle_info.data)

    def test_list_vehicle(self):
        response = self.client.get(
            reverse('list_vehicle_info'),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        vehicle_list = VechicleInfo.objects.all()
        vehicle_serialiser = VechicleInfoSerializer(
            vehicle_list, many=True)
        self.assertEquals(response.json(), vehicle_serialiser.data)

    def test_delete_vehicle(self):
        response = self.client.delete(
            reverse('delete_vehicle_info', kwargs={
                    'vehicle_id': self.vehicle_info.vehicle_id}),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
