from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from vehicle.models import VechicleInfo
from vehicle.serializers import (VechicleInfoCreateSerializer,
                                 VechicleInfoSerializer)


class VechicleInfoCreateAPI(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = VechicleInfo.objects.all()
    serializer_class = VechicleInfoCreateSerializer


class VechicleInfoListAPI(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = VechicleInfo.objects.all()
    serializer_class = VechicleInfoSerializer


class GetSingleVechicleInfoAPI(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = VechicleInfo.objects.all()
    serializer_class = VechicleInfoSerializer
    lookup_field = "vehicle_id"


class VechicleInfoUpdateAPI(generics.RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = VechicleInfo.objects.all()
    serializer_class = VechicleInfoCreateSerializer
    lookup_field = "vehicle_id"


class VechicleInfoDeleteAPI(generics.DestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = VechicleInfo.objects.all()
    serializer_class = VechicleInfoSerializer
    lookup_field = "vehicle_id"

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(data={"message": "Deleted"}, status=status.HTTP_204_NO_CONTENT)
