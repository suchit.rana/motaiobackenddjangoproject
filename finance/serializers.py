from rest_framework import serializers

from finance.models import Finance, UserDetails


class UserDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserDetails
        fields = '__all__'


class FinanceCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Finance
        fields = '__all__'


class FinanceSerializer(serializers.ModelSerializer):
    borrower_details = UserDetailsSerializer(many=False)
    guarantor = UserDetailsSerializer(many=False)

    class Meta:
        model = Finance
        fields = '__all__'
