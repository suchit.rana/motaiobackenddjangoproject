from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = []

CORS_ALLOWED_ORIGINS = []

SECURE_CONTENT_TYPE_NOSNIFF = True

SECURE_BROWSER_XSS_FILTER = True

X_FRAME_OPTIONS = 'DENY'

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

SECURE_SSL_REDIRECT = True

SECURE_HSTS_PRELOAD = True

SECURE_HSTS_SECONDS = 31536000

SECURE_HSTS_INCLUDE_SUBDOMAINS = True

CSP_DEFAULT_SRC = (
    "'self'",
)

CSP_STYLE_SRC = (
    "'self'",
)

CSP_SCRIPT_SRC = (
    "'self'",
)

CSP_IMG_SRC = ("'self'", )

CSP_FONT_SRC = ("'self'", )

CSP_BASE_URI = ("'self'", )

CSP_FRAME_ANCESTORS = ("'self'", )

CSP_FRAME_SRC = ("'self'", )

CSP_FORM_ACTION = ("'self'", )
