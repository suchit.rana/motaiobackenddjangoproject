import json

from django.test import Client, TestCase
from django.urls import reverse
from rest_framework import status

from authentication.models import User
from organisation.models import Organisation
from organisation.serializers import OrganisationSerializer
from utility.utils import generate_organisation_id, generate_user_id


class OrganisationTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.user = User.objects.create(
            user_id=generate_user_id(),
            username="johndoe@gmail.com",
            email="johndoe@gmail.com",
            first_name="John",
            last_name="Doe",
            business_name="XYZ and sons"
        )
        self.user.set_password("john$@Doe")
        self.user.save()
        #
        self.client.login(username='johndoe@gmail.com', password='john$@Doe')
        self.organisation = Organisation.objects.create(
            org_id=generate_organisation_id(),
            organisation_name="John Doe Organisation 001",
            organisation_address="23 John Drive",
            primary_employee=self.user
        )

    def test_create_organisation(self):
        response = self.client.post(
            reverse('create_organisation'),
            data=json.dumps({
                "organisation_name": "John Doe Organisation",
                "organisation_address": "23 John Doe Street",
                "primary_employee": self.user.user_id,
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        json_data = response.json()
        self.assertIn('org_id', json_data)

    def test_update_organisation(self):
        response = self.client.put(
            reverse('update_organisation', kwargs={
                    'org_id': self.organisation.org_id}),
            data=json.dumps({
                "organisation_name": "John Doe Organisation Update",
                "organisation_address": "23 John Doe Street",
                "primary_employee": self.user.user_id,
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        json_data = response.json()
        self.assertIn('org_id', json_data)
        self.assertIn('organisation_name', json_data)
        self.assertEquals(json_data['organisation_name'],
                          'John Doe Organisation Update')

    def test_get_organisation(self):
        response = self.client.get(
            reverse('get_single_organisation', kwargs={
                    'org_id': self.organisation.org_id}),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.organisation.refresh_from_db()
        organisation_data = OrganisationSerializer(
            self.organisation, many=False)
        self.assertDictEqual(response.json(), organisation_data.data)

    def test_list_organisation(self):
        response = self.client.get(
            reverse('list_organisation'),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        organisation_list = Organisation.objects.all()
        organisation_data = OrganisationSerializer(
            organisation_list, many=True)
        self.assertEquals(response.json(), organisation_data.data)

    def test_delete_organisation(self):
        response = self.client.delete(
            reverse('delete_organisation', kwargs={
                    'org_id': self.organisation.org_id}),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
