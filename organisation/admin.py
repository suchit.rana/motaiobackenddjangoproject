from django.contrib import admin

from organisation.models import Organisation


class OrganisationAdmin(admin.ModelAdmin):
    list_display = ('org_id', 'organisation_name', 'organisation_address',
                    'primary_employee', 'created_at', 'updated_at')


admin.site.register(Organisation, OrganisationAdmin)
