from djongo import models

from customer.models import MARITAL_CHOICES
from utility.utils import generate_finance_id, generate_user_id


class UserDetails(models.Model):

    customer_id = models.CharField(
        primary_key=True, unique=True, max_length=30, default=generate_user_id)
    salutation = models.CharField(max_length=20)
    first_name = models.CharField(max_length=100)
    middle_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    postal_address = models.TextField(blank=True)
    home_phone = models.CharField(max_length=20, blank=True)
    work_phone = models.CharField(max_length=20, blank=True)
    mobile_phone = models.CharField(max_length=20, blank=True)
    fax = models.CharField(max_length=50, blank=True)
    email_address = models.CharField(max_length=500, blank=True)
    mz_resident = models.BooleanField(default=False)
    date_of_birth = models.DateField(blank=True)
    country_of_birth = models.CharField(max_length=300, blank=True)
    citizenship_1 = models.CharField(max_length=100, blank=True)
    citizenship_2 = models.CharField(max_length=100, blank=True)
    driver_license_no = models.CharField(max_length=50, blank=True)
    version = models.CharField(max_length=20, blank=True)
    #
    residence = models.CharField(max_length=50, blank=True)
    residence_duration = models.PositiveIntegerField(blank=True)
    previous_address = models.TextField(blank=True)
    years_in_prev_add = models.PositiveIntegerField(blank=True)
    marital_status = models.CharField(
        choices=MARITAL_CHOICES, blank=True, max_length=15)

    dependants = models.CharField(blank=True, max_length=200)
    ages = models.CharField(blank=True, max_length=20)
    #
    current_employer = models.CharField(max_length=100, blank=True)
    current_employer_address = models.TextField(blank=True)
    how_long = models.PositiveIntegerField(blank=True)
    previous_occupation = models.CharField(blank=True, max_length=200)
    #
    previous_employer = models.CharField(max_length=100, blank=True)
    previous_employer_address = models.TextField(blank=True)
    how_long = models.PositiveIntegerField(blank=True)
    nearest_relative_address = models.TextField(blank=True)
    #
    telephone = models.CharField(blank=True, max_length=20)
    relationship = models.CharField(blank=True, max_length=100)
    credit_ref = models.CharField(blank=True, max_length=200)
    bank_and_branch = models.CharField(blank=True, max_length=100)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "User Details"
        verbose_name_plural = "User Details"


class Finance(models.Model):

    finance_id = models.CharField(
        primary_key=True, unique=True, max_length=30, default=generate_finance_id)
    borrower_details = models.ForeignKey(
        to=UserDetails, on_delete=models.CASCADE, related_name="borrower")
    guarantor_details = models.ForeignKey(
        to=UserDetails, on_delete=models.CASCADE, related_name="guarantor")
    transaction_details = models.JSONField(default=dict)
    financial_details = models.JSONField(default=dict)
    balance_paid_by = models.CharField(max_length=500)
    monthly_installments = models.DecimalField(
        blank=True, max_digits=15, decimal_places=2)
    final_installment = models.DecimalField(
        blank=True, max_digits=15, decimal_places=2)
    annual_interest = models.DecimalField(
        blank=True, max_digits=15, decimal_places=2)
    borrowers_financials_1 = models.JSONField(default=dict)
    borrowers_financials_2 = models.JSONField(default=dict)

    class Meta:
        verbose_name = "Finance"
        verbose_name_plural = "Finances"
