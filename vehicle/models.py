
from django import forms
from djongo import models

from utility.utils import generate_vehicle_id

TRANSMISSION_CHOICES = (
    ("Automatic", "Automatic"),
    ("Manual", "Manual")
)


class VechiclePurchaseInfo(models.Model):

    purchase_date = models.DateField(blank=False)
    purchase_type = models.CharField(blank=True, max_length=200)
    supplier = models.CharField(blank=True, max_length=200)
    purchase_note = models.TextField()
    auction_grade = models.CharField(blank=True, max_length=50)
    vehicle_appraisal = models.TextField()
    appraised_by = models.CharField(blank=True, max_length=200)
    note = models.TextField()
    deliver_port = models.CharField(blank=True, max_length=200)
    owner_type = models.CharField(blank=True, max_length=200)
    year_first_registered = models.CharField(blank=True, max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Vehicle Purchase Information"
        verbose_name_plural = "Vehicle Purchase Information"
        ordering = ('purchase_date',)


class VehicleAdditionalInfo(models.Model):

    primary_color = models.CharField(blank=True, max_length=50)
    secondary_color = models.CharField(blank=True, max_length=50)
    doors = models.PositiveIntegerField(blank=True)
    sun_proof = models.BooleanField(default=False)
    wheel_size = models.PositiveIntegerField(blank=True)
    wheel_composition = models.CharField(blank=True, max_length=50)
    wheel_condition = models.CharField(blank=True, max_length=50)
    # int
    interior_color = models.CharField(blank=True, max_length=50)
    interior_trim = models.CharField(blank=True, max_length=50)
    no_of_seats = models.PositiveIntegerField(blank=True)
    interior_material = models.CharField(blank=True, max_length=50)
    reciever_capability = models.BooleanField(default=False)
    no_of_airbags = models.PositiveIntegerField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Vehicle Additional Info"
        verbose_name_plural = "Vehicle Additional Info"
        ordering = ('created_at',)


class VehicleMechanicalInfo(models.Model):

    engine_size = models.DecimalField(
        blank=True, max_digits=15, decimal_places=2)
    engine_size_units = models.CharField(max_length=10)
    engine_type = models.CharField(max_length=50)
    primary_fuel_type = models.CharField(max_length=100)
    no_of_cylinders = models.PositiveIntegerField(blank=True)
    transmission = models.CharField(
        blank=False, choices=TRANSMISSION_CHOICES, max_length=15)
    turbo = models.BooleanField(default=False)
    drive_system = models.CharField(max_length=100)
    battery_power = models.PositiveBigIntegerField(blank=True)
    range = models.DecimalField(blank=True, max_digits=15, decimal_places=2)
    soh = models.PositiveIntegerField(blank=True)
    odometer_status = models.CharField(max_length=100)
    latest_reading = models.DecimalField(
        blank=True, max_digits=15, decimal_places=2)
    odometer_certified = models.BooleanField(default=False)
    abs_braking = models.BooleanField(default=False)
    traction_control = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Vehicle Mechanical Info"
        verbose_name_plural = "Vehicle Mechanical Info"
        ordering = ('created_at',)


class VechicleAdministration(models.Model):

    status = models.CharField(blank=True, max_length=100)
    status_comment = models.TextField()
    notes = models.TextField()
    vehicle_group = models.CharField(blank=True, max_length=100)
    retail_price = models.DecimalField(
        blank=True, max_digits=15, decimal_places=2)
    wholesale_price = models.DecimalField(
        blank=True, max_digits=15, decimal_places=2)
    special_price = models.DecimalField(
        blank=True, max_digits=15, decimal_places=2)
    on_road_price = models.DecimalField(
        blank=True, max_digits=15, decimal_places=2)
    disable_finance = models.BooleanField(default=False)
    license_purchase_date = models.DateTimeField()
    road_user_charges = models.BooleanField(default=False)
    outstanding_road_user_charges = models.BooleanField(default=False)
    ruc_end_distance = models.CharField(blank=True, max_length=100)
    wof_cof_expiry = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Vehicle Image"
        verbose_name_plural = "Vehicle Images"
        ordering = ('created_at',)


class VechicleExpense(models.Model):

    id = models.ObjectIdField()
    expense_type = models.CharField(blank=True, max_length=200)
    part = models.CharField(blank=True, max_length=200)
    supplier = models.CharField(blank=True, max_length=200)
    ref_no = models.CharField(blank=True, max_length=200)
    category = models.CharField(blank=True, max_length=200)
    date = models.DateField(blank=True)
    dealership = models.CharField(blank=True, max_length=200)
    description = models.TextField()
    expense = models.CharField(blank=True, max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Vehicle Expense"
        verbose_name_plural = "Vehicle Expenses"
        ordering = ('date',)


class VechicleExpenseForm(forms.ModelForm):
    class Meta:
        model = VechicleExpense
        fields = '__all__'


class VechicleImage(models.Model):

    id = models.ObjectIdField()
    vehicle_image = models.URLField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Vehicle Image"
        verbose_name_plural = "Vehicle Images"
        ordering = ('created_at',)


class VechicleImageForm(forms.ModelForm):
    class Meta:
        model = VechicleImage
        fields = '__all__'


class VechicleInfo(models.Model):

    vehicle_id = models.CharField(
        primary_key=True, unique=True, max_length=30, default=generate_vehicle_id)
    stock_code = models.CharField(blank=True, max_length=200)
    manufacturer = models.CharField(blank=True, max_length=200)
    model = models.CharField(blank=True, max_length=200)
    year = models.CharField(blank=True, max_length=4)
    registration_plate = models.CharField(blank=True, max_length=50)
    vin_number = models.CharField(blank=True, max_length=200)
    chassis_no = models.CharField(blank=True, max_length=200)
    engine_no = models.CharField(blank=True, max_length=200)
    purchase_info = models.OneToOneField(
        to=VechiclePurchaseInfo, on_delete=models.CASCADE, null=True)
    additonal_vehicle_info = models.OneToOneField(
        to=VehicleAdditionalInfo, on_delete=models.CASCADE, null=True)
    mechanical_info = models.OneToOneField(
        to=VehicleMechanicalInfo, on_delete=models.CASCADE, null=True)
    vehicle_admin = models.OneToOneField(
        to=VechicleAdministration, on_delete=models.CASCADE, null=True)
    vehicle_expenses = models.ManyToManyField(to=VechicleExpense, blank=True)
    vehicle_images = models.ManyToManyField(to=VechicleImage, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Vehicle Information"
        verbose_name_plural = "Vehicle Information"
        ordering = ('stock_code',)
