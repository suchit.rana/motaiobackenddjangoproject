from django.urls import path
from authentication.views import RegisterView
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token


urlpatterns = [
    path('login', obtain_jwt_token, name='user_authentication'),
    path('token-refresh', refresh_jwt_token, name='refresh_token'),
    path('register', RegisterView.as_view(), name='auth_register'),
]
