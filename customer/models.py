from djongo import models

from authentication.models import User
from utility.utils import (generate_address_id, generate_customer_id,
                           generate_test_drive_id)
from vehicle.models import VechicleInfo

GENDER_CHOICES = (
    ("Male", "Male"),
    ("Female", "Female"),
    ("Unspecified", "Unspecified"),
)

MARITAL_CHOICES = (
    ("Single", "Single"),
    ("Married", "Married"),
    ("Divorced", "Divorced"),
    ("Unspecified", "Unspecified"),
)


class CustomerInfo(models.Model):

    customer_id = models.CharField(
        primary_key=True, unique=True, max_length=30, default=generate_customer_id)
    salutation = models.CharField(max_length=20)
    first_name = models.CharField(max_length=100)
    middle_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    preferred_name = models.CharField(max_length=100, blank=True)
    initials = models.CharField(max_length=100, blank=True)
    trading_as = models.CharField(max_length=100, blank=True)
    date_of_birth = models.DateField(blank=True)
    driver_license_no = models.CharField(max_length=50, blank=True)
    driver_license_version = models.CharField(max_length=50, blank=True)
    gender = models.CharField(choices=GENDER_CHOICES,
                              default="Unspecified", blank=True, max_length=30)
    marital_status = models.CharField(
        choices=MARITAL_CHOICES, default="Unspecified", blank=True, max_length=30)
    #
    employer = models.CharField(max_length=500, blank=True)
    job_title = models.CharField(max_length=500, blank=True)
    #
    home_phone = models.CharField(max_length=20, blank=True)
    work_phone = models.CharField(max_length=20, blank=True)
    mobile_phone = models.CharField(max_length=20, blank=True)
    fax = models.CharField(max_length=50, blank=True)
    email_address = models.CharField(max_length=500, blank=True)
    #
    referral_method = models.CharField(max_length=50, blank=True)
    #
    created_by = models.ForeignKey(
        to=User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Customer Information"
        verbose_name_plural = "Customer Information"
        ordering = ('first_name', 'last_name',)

    def __str__(self):
        return f"{self.salutation} {self.first_name} {self.last_name}"


class CustomerAddress(models.Model):

    address_id = models.CharField(
        primary_key=True, unique=True, max_length=30, default=generate_address_id)
    customer = models.ForeignKey(to=CustomerInfo, on_delete=models.CASCADE)
    address = models.TextField()
    suburb = models.CharField(blank=True, max_length=200)
    city = models.CharField(blank=True, max_length=200)
    postcode = models.CharField(blank=True, max_length=200)
    country = models.CharField(blank=True, max_length=300)
    postal_address = models.TextField()
    postal_suburb = models.CharField(blank=True, max_length=200)
    postal_city = models.CharField(blank=True, max_length=200)
    postal_postcode = models.CharField(blank=True, max_length=200)
    postal_country = models.CharField(blank=True, max_length=300)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Customer Address"
        verbose_name_plural = "Customer Address"

    def __str__(self):
        return self.address_id


class TestDrive(models.Model):

    id = models.CharField(
        primary_key=True, unique=True, max_length=30, default=generate_test_drive_id)
    customer = models.ForeignKey(to=CustomerInfo, on_delete=models.CASCADE)
    vehicle = models.ForeignKey(to=VechicleInfo, on_delete=models.CASCADE)
    #
    activity_no = models.CharField(blank=True, max_length=200)
    referral = models.CharField(blank=True, max_length=200)
    dealership = models.CharField(blank=True, max_length=200)
    sales_person = models.CharField(blank=True, max_length=200)
    # oppurtunity details
    next_action = models.CharField(blank=True, max_length=200)
    next_action_date = models.DateTimeField(auto_now=True)
    # test drive details
    date_out = models.DateTimeField(auto_now=True)
    time_out = models.TimeField()
    date_due = models.DateTimeField(auto_now=True)
    time_due = models.TimeField()
    #
    comment = models.TextField()
    #
    driver_license_front = models.URLField(blank=True)
    driver_license_back = models.URLField(blank=True)
    #
    signature = models.CharField(blank=True, max_length=500)
    #
    date_returned = models.DateTimeField(auto_now=True)
    time_returned = models.TimeField()
    #
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Customer Address"
        verbose_name_plural = "Customer Address"

    def __str__(self):
        return self.activity_no
