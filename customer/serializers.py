from rest_framework import serializers

from authentication.serializers import UserSerializer
from customer.models import CustomerAddress, CustomerInfo, TestDrive


class CustomerInfoCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerInfo
        fields = '__all__'
        extra_kwargs = {
            'created_by': {'required': False}
        }


class CustomerInfoSerializer(serializers.ModelSerializer):
    created_by = UserSerializer(many=False)

    class Meta:
        model = CustomerInfo
        fields = '__all__'


class CustomerAddressSerializer(serializers.ModelSerializer):

    class Meta:
        model = CustomerAddress
        fields = ('address_id', 'address', 'suburb', 'city', 'postcode', 'country', 'postal_address',
                  'postal_suburb', 'postal_city', 'postal_postcode', 'postal_country', 'created_at', 'updated_at')


class CustomerAddressCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerAddress
        fields = '__all__'

class TestDriveCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestDrive
        fields = '__all__'

class TestDriveSerializer(serializers.ModelSerializer):

    class Meta:
        model = TestDrive
        fields = ()
    