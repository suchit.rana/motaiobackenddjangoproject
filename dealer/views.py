from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from dealer.models import Dealer
from dealer.serializers import DealerCreateSerializer, DealerSerializer


class DealerCreateAPI(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Dealer.objects.all()
    serializer_class = DealerCreateSerializer


class DealerListAPI(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Dealer.objects.all()
    serializer_class = DealerSerializer


class GetSingleDealerAPI(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Dealer.objects.all()
    serializer_class = DealerSerializer
    lookup_field = "dealer_id"


class DealerUpdateAPI(generics.RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Dealer.objects.all()
    serializer_class = DealerCreateSerializer
    lookup_field = "dealer_id"


class DealerDeleteAPI(generics.DestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Dealer.objects.all()
    serializer_class = DealerSerializer
    lookup_field = "dealer_id"

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(data={"message": "Deleted"}, status=status.HTTP_204_NO_CONTENT)
