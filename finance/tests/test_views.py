import json
from decimal import Decimal

from django.test import Client, TestCase
from django.urls import reverse
from rest_framework import status

from authentication.models import User
from finance.models import Finance, UserDetails
from finance.serializers import FinanceSerializer
from utility.utils import (generate_customer_id, generate_finance_id,
                           generate_user_id)


class FinanceTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.user = User.objects.create(
            user_id=generate_user_id(),
            username="johndoe@gmail.com",
            email="johndoe@gmail.com",
            first_name="John",
            last_name="Doe",
            business_name="XYZ and sons"
        )
        self.user.set_password("john$@Doe")
        self.user.save()
        #
        self.client.login(username='johndoe@gmail.com', password='john$@Doe')
        self.user_details = UserDetails.objects.create(
            customer_id=generate_customer_id(),
            salutation="Mr",
            first_name="John",
            middle_name="Doe",
            last_name="D",
        )
        self.user_details_1 = UserDetails.objects.create(
            customer_id=generate_customer_id(),
            salutation="Mr",
            first_name="John1",
            middle_name="Doe1",
            last_name="D1",
        )
        self.finance = Finance.objects.create(
            finance_id=generate_finance_id(),
            borrower_details=self.user_details,
            guarantor_details=self.user_details_1,
            transaction_details={},
            financial_details={},
            balance_paid_by="Mimi",
            monthly_installments=Decimal(1500),
            final_installment=Decimal(1500),
            annual_interest=Decimal(15),
            borrowers_financials_1={},
            borrowers_financials_2={}
        )

    def test_create_finance(self):
        response = self.client.post(
            reverse('create_finance'),
            data=json.dumps({
                {
                    "borrower_details": self.user_details_1.customer_id,
                    "guarantor_details": self.user_details.customer_id,
                    "balance_paid_by": "Mimi",
                    "monthly_installments": 15000.0,
                    "final_installment": 30000.0,
                    "annual_interest": 15
                }
            }),
            content_type='application/json'
        )
        print(response.json())
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_finance(self):
        response = self.client.put(
            reverse('update_finance', kwargs={
                    'finance_id': self.finance.finance_id}),
            data=json.dumps({
                "borrower_details": self.user_details.customer_id,
                "guarantor_details": self.user_details_1.customer_id,
                "balance_paid_by": "User 001",
                "monthly_installments": 35000.0,
                "final_installment": 30000.0,
                "annual_interest": 20
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        json_data = response.json()
        self.assertIn('monthly_installments', json_data)
        self.assertEquals(json_data['monthly_installments'], '35000.00')

    def test_get_finance(self):
        response = self.client.get(
            reverse('get_finance', kwargs={
                    'finance_id': self.finance.finance_id}),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.finance.refresh_from_db()
        finance_serializer = FinanceSerializer(
            self.finance, many=False)
        self.assertDictEqual(response.json(), finance_serializer.data)

    def test_list_finance(self):
        response = self.client.get(
            reverse('list_finance'),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        finance_list = Finance.objects.all()
        finance_serialiser = FinanceSerializer(
            finance_list, many=True)
        self.assertEquals(response.json(), finance_serialiser.data)

    def test_delete_finance(self):
        response = self.client.delete(
            reverse('delete_finance', kwargs={
                    'finance_id': self.finance.finance_id}),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
