from django.urls import path

from organisation.views import (GetSingleOrganisationAPI,
                                OrganisationCreateAPI, OrganisationDeleteAPI,
                                OrganisationListAPI, OrganisationUpdateAPI)

urlpatterns = [
    path('create', OrganisationCreateAPI.as_view(), name="create_organisation"),
    path('list', OrganisationListAPI.as_view(), name="list_organisation"),
    path('update/<str:org_id>', OrganisationUpdateAPI.as_view(),
         name="update_organisation"),
    path('delete/<str:org_id>', OrganisationDeleteAPI.as_view(),
         name="delete_organisation"),
    path('<str:org_id>', GetSingleOrganisationAPI.as_view(),
         name="get_single_organisation"),
]
