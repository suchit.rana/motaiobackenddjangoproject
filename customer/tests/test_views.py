import json

from django.test import Client, TestCase
from django.urls import reverse
from rest_framework import status

from authentication.models import User
from customer.models import CustomerInfo
from customer.serializers import CustomerInfoSerializer
from utility.utils import generate_customer_id, generate_user_id


class CustomerTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.user = User.objects.create(
            user_id=generate_user_id(),
            username="johndoe@gmail.com",
            email="johndoe@gmail.com",
            first_name="John",
            last_name="Doe",
            business_name="XYZ and sons"
        )
        self.user.set_password("john$@Doe")
        self.user.save()
        #
        self.client.login(username='johndoe@gmail.com', password='john$@Doe')
        self.customer = CustomerInfo.objects.create(
            customer_id=generate_customer_id(),
            salutation="Mr",
            first_name="John",
            middle_name="Doe",
            last_name="D",
            created_by=self.user
        )

    def test_create_customer(self):
        response = self.client.post(
            reverse('create_customer'),
            data=json.dumps({
                "salutation": "Mr",
                "first_name": "Micheal",
                "middle_name": "Jude",
                "last_name": "Jackson",
                "created_by": self.user.user_id,
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_customer(self):
        response = self.client.put(
            reverse('update_customer', kwargs={
                    'customer_id': self.customer.customer_id}),
            data=json.dumps({
                "salutation": "Mr",
                "first_name": "John",
                "middle_name": "Doe",
                "last_name": "Updated",
                "created_by": self.user.user_id,
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        json_data = response.json()
        self.assertIn('last_name', json_data)
        self.assertEquals(json_data['last_name'], 'Updated')

    def test_get_customer(self):
        response = self.client.get(
            reverse('get_customer', kwargs={
                    'customer_id': self.customer.customer_id}),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.customer.refresh_from_db()
        customer_data = CustomerInfoSerializer(
            self.customer, many=False)
        self.assertDictEqual(response.json(), customer_data.data)

    def test_list_customer(self):
        response = self.client.get(
            reverse('list_customer'),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        customer_list = CustomerInfo.objects.all()
        customer_serialiser = CustomerInfoSerializer(
            customer_list, many=True)
        self.assertEquals(response.json(), customer_serialiser.data)

    def test_delete_customer(self):
        response = self.client.delete(
            reverse('delete_customer', kwargs={
                    'customer_id': self.customer.customer_id}),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
