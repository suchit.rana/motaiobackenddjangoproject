from rest_framework import serializers

from authentication.serializers import UserSerializer
from dealer.models import Dealer
from organisation.serializers import OrganisationSerializer


class DealerSerializer(serializers.ModelSerializer):
    primary_employee = UserSerializer(many=False)
    organisation = OrganisationSerializer(many=False)

    class Meta:
        model = Dealer
        fields = ('dealer_id', 'dealer_name', 'dealer_address',
                  'primary_employee', 'organisation', 'created_at', 'updated_at')


class DealerCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dealer
        fields = '__all__'
