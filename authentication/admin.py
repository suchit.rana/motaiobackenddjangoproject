from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from authentication.models import User


class CustomUserAdmin(UserAdmin):
    list_display = ('email', 'first_name', 'last_name',
                    'business_name', 'is_staff')


admin.site.register(User, CustomUserAdmin)
