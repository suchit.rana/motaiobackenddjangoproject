from django.urls import path

from dealer.views import (DealerCreateAPI, DealerDeleteAPI, DealerListAPI,
                          DealerUpdateAPI, GetSingleDealerAPI)

urlpatterns = [
    path('create', DealerCreateAPI.as_view(), name="create_dealer"),
    path('list', DealerListAPI.as_view(), name="list_dealer"),
    path('update/<str:dealer_id>', DealerUpdateAPI.as_view(), name="update_dealer"),
    path('delete/<str:dealer_id>', DealerDeleteAPI.as_view(), name="delete_dealer"),
    path('<str:dealer_id>', GetSingleDealerAPI.as_view(), name="get_dealer"),
]
