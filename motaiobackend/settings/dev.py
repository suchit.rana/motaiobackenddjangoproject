from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = [
    '*',
    '192.168.1.213'
]

CORS_ALLOWED_ORIGINS = [
    "http://localhost",
]
