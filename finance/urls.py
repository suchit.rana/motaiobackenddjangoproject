from django.urls import path

from finance.views import (FinanceCreateAPI, FinanceDeleteAPI, FinanceListAPI,
                           FinanceUpdateAPI, GetSingleFinanceAPI,
                           UserDetailsCreateAPI)

urlpatterns = [
    path('user/create', UserDetailsCreateAPI.as_view(),
         name='create_finance_user'),
    path('create', FinanceCreateAPI.as_view(), name="create_finance"),
    path('list', FinanceListAPI.as_view(), name="list_finance"),
    path('update/<str:finance_id>',
         FinanceUpdateAPI.as_view(), name="update_finance"),
    path('delete/<str:finance_id>',
         FinanceDeleteAPI.as_view(), name="delete_finance"),
    path('<str:finance_id>', GetSingleFinanceAPI.as_view(), name="get_finance"),
]
