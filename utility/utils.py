import random
import string

from django.contrib.auth.backends import ModelBackend
from rest_framework.views import exception_handler

from authentication.models import User
from authentication.serializers import UserSerializer


def jwt_response_payload_handler(token, user=None, request=None) -> dict:
    return {
        'token': token,
        'data': UserSerializer(user).data
    }


def generate_user_id() -> str:
    return ''.join(random.choice(string.ascii_letters) for i in range(25))


def generate_organisation_id() -> str:
    random_string = ''.join(random.choice(string.ascii_letters)
                            for i in range(20))
    return f"org_{random_string}"


def generate_dealer_id() -> str:
    random_string = ''.join(random.choice(string.ascii_letters)
                            for i in range(20))
    return f"deal_{random_string}"


def generate_customer_id() -> str:
    random_string = ''.join(random.choice(string.ascii_letters)
                            for i in range(10))
    return f"cus_{random_string}"


def generate_address_id() -> str:
    random_string = ''.join(random.choice(string.ascii_letters)
                            for i in range(5))
    return f"add_{random_string}"


def generate_test_drive_id() -> str:
    random_string = ''.join(random.choice(string.ascii_letters)
                            for i in range(5))
    return f"tdrive_{random_string}"


def generate_vehicle_id() -> str:
    random_string = ''.join(random.choice(string.ascii_letters)
                            for i in range(5))
    return f"veh_{random_string}"


def generate_finance_id() -> str:
    random_string = ''.join(random.choice(string.ascii_letters)
                            for i in range(5))
    return f"fin_{random_string}"


class EmailBackend(ModelBackend):

    def authenticate(self, request, username=None, password=None, **kwargs):
        try:
            if username:
                user = User.objects.get(username=username)
            else:
                user = User.objects.get(email=kwargs['email'])
        except User.DoesNotExist:
            return None
        else:
            if user.check_password(password):
                return user
        return None


def custom_exception_handler(exc, context):
    response = exception_handler(exc, context)
    data = {}
    data['error_code'] = response.status_code  # type: ignore
    data['message'] = response.status_text  # type: ignore
    data['errors'] = response.data or []  # type: ignore
    response.data = data  # type: ignore
    return response
