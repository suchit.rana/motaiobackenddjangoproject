import json

from django.test import Client, TestCase
from django.urls import reverse
from rest_framework import status

from authentication.models import User
from dealer.models import Dealer
from dealer.serializers import DealerSerializer
from organisation.models import Organisation
from utility.utils import (generate_dealer_id, generate_organisation_id,
                           generate_user_id)


class DealerTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.user = User.objects.create(
            user_id=generate_user_id(),
            username="johndoe@gmail.com",
            email="johndoe@gmail.com",
            first_name="John",
            last_name="Doe",
            business_name="XYZ and sons"
        )
        self.user.set_password("john$@Doe")
        self.user.save()
        #
        self.client.login(username='johndoe@gmail.com', password='john$@Doe')
        self.organisation = Organisation.objects.create(
            org_id=generate_organisation_id(),
            organisation_name="John Doe Organisation 001",
            organisation_address="23 John Drive",
            primary_employee=self.user
        )
        self.dealer = Dealer.objects.create(
            dealer_id=generate_dealer_id(),
            dealer_name="Dealer 001",
            dealer_address="Dealer Address",
            primary_employee=self.user,
            organisation=self.organisation
        )

    def test_create_dealer(self):
        response = self.client.post(
            reverse('create_dealer'),
            data=json.dumps({
                "dealer_name": "John Doe Dealer",
                "dealer_address": "23 John Doe Street",
                "primary_employee": self.user.user_id,
                "organisation": self.organisation.org_id
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        json_data = response.json()
        self.assertIn('dealer_id', json_data)

    def test_update_dealer(self):
        response = self.client.put(
            reverse('update_dealer', kwargs={
                    'dealer_id': self.dealer.dealer_id}),
            data=json.dumps({
                "dealer_name": "John Doe Dealer Update",
                "dealer_address": "Dealer Address",
                "primary_employee": self.user.user_id,
                "organisation": self.organisation.org_id
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        json_data = response.json()
        self.assertIn('dealer_id', json_data)
        self.assertIn('dealer_name', json_data)
        self.assertEquals(json_data['dealer_name'],
                          'John Doe Dealer Update')

    def test_get_dealer(self):
        response = self.client.get(
            reverse('get_dealer', kwargs={
                    'dealer_id': self.dealer.dealer_id}),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.dealer.refresh_from_db()
        dealer_data = DealerSerializer(
            self.dealer, many=False)
        self.assertDictEqual(response.json(), dealer_data.data)

    def test_list_dealer(self):
        response = self.client.get(
            reverse('list_dealer'),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        dealer_list = Dealer.objects.all()
        dealer_data = DealerSerializer(
            dealer_list, many=True)
        self.assertEquals(response.json(), dealer_data.data)

    def test_delete_dealer(self):
        response = self.client.delete(
            reverse('delete_dealer', kwargs={
                    'dealer_id': self.dealer.dealer_id}),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
