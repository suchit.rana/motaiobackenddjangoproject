from rest_framework import serializers

from authentication.models import User
from authentication.serializers import UserSerializer
from organisation.models import Organisation


class OrganisationSerializer(serializers.ModelSerializer):
    primary_employee = UserSerializer(many=False)

    class Meta:
        model = Organisation
        fields = ('org_id', 'organisation_name', 'organisation_address',
                  'primary_employee', 'created_at', 'updated_at')


class OrganisationCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organisation
        fields = '__all__'
