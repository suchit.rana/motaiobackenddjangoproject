import json

from django.test import Client, TestCase
from django.urls import reverse
from rest_framework import status

from authentication.models import User
from utility.utils import generate_user_id


class LoginTest(TestCase):

    def setUp(self):
        self.client = Client()
        user = User.objects.create(
            user_id=generate_user_id(),
            username="johndoe@gmail.com",
            email="johndoe@gmail.com",
            first_name="John",
            last_name="Doe",
            business_name="XYZ and sons"
        )
        user.set_password("john$@Doe")
        user.save()

    def test_valid_login(self):
        response = self.client.post(
            reverse('user_authentication'),
            data=json.dumps({
                "email": "johndoe@gmail.com",
                "password": "john$@Doe"
            }),
            content_type='application/json'
        )
        self.assertIn("token", response.json())
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_login(self):
        response = self.client.post(
            reverse('auth_register'),
            data=json.dumps({
                "email": "johndoe@gmail.com",
                "password": "john$6373"
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code,
                         status.HTTP_422_UNPROCESSABLE_ENTITY)


class RegisterViewTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.valid_payload = {
            "first_name": "John",
            "last_name": "Doe",
            "business_name": "XYZ and sons",
            "email": "johndoe@gmail.com",
            "password": "john$@Doe",
            "confirm_password": "john$@Doe"
        }
        self.invalid_payload = {
            "first_name": "",
            "last_name": "Doe",
            "business_name": "XYZ and sons",
            "email": "johndoe@gmail.com",
            "password": "john$@Doe"
        }

    def test_valid_registration(self):
        response = self.client.post(
            reverse('auth_register'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_invalid_registration(self):
        response = self.client.post(
            reverse('auth_register'),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code,
                         status.HTTP_422_UNPROCESSABLE_ENTITY)
