from djongo import models

from authentication.models import User
from utility.utils import generate_organisation_id


class Organisation(models.Model):

    org_id = models.CharField(
        primary_key=True, unique=True, max_length=30, default=generate_organisation_id)
    organisation_name = models.CharField(max_length=500)
    organisation_address = models.TextField()
    primary_employee = models.ForeignKey(
        to=User, on_delete=models.CASCADE, related_name="org_primary_employee")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Organisation"
        verbose_name_plural = "Organisations"
        ordering = ('organisation_name',)

    def __str__(self):
        return self.organisation_name
