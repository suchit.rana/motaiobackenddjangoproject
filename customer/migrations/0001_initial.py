# Generated by Django 3.2.11 on 2022-01-22 12:51

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models

import utility.utils


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('vehicle', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomerInfo',
            fields=[
                ('customer_id', models.CharField(default=utility.utils.generate_customer_id,
                                                 max_length=30, primary_key=True, serialize=False, unique=True)),
                ('salutation', models.CharField(max_length=20)),
                ('first_name', models.CharField(max_length=100)),
                ('middle_name', models.CharField(max_length=100)),
                ('last_name', models.CharField(max_length=100)),
                ('preferred_name', models.CharField(blank=True, max_length=100)),
                ('initials', models.CharField(blank=True, max_length=100)),
                ('trading_as', models.CharField(blank=True, max_length=100)),
                ('date_of_birth', models.DateField(blank=True)),
                ('driver_license_no', models.CharField(blank=True, max_length=50)),
                ('driver_license_version', models.CharField(
                    blank=True, max_length=50)),
                ('gender', models.CharField(blank=True, choices=[('Male', 'Male'), ('Female', 'Female'), (
                    'Unspecified', 'Unspecified')], default='Unspecified', max_length=30)),
                ('marital_status', models.CharField(blank=True, choices=[('Single', 'Single'), ('Married', 'Married'), (
                    'Divorced', 'Divorced'), ('Unspecified', 'Unspecified')], default='Unspecified', max_length=30)),
                ('employer', models.CharField(blank=True, max_length=500)),
                ('job_title', models.CharField(blank=True, max_length=500)),
                ('home_phone', models.CharField(blank=True, max_length=20)),
                ('work_phone', models.CharField(blank=True, max_length=20)),
                ('mobile_phone', models.CharField(blank=True, max_length=20)),
                ('fax', models.CharField(blank=True, max_length=50)),
                ('email_address', models.CharField(blank=True, max_length=500)),
                ('referral_method', models.CharField(blank=True, max_length=50)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_by', models.ForeignKey(
                    on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Customer Information',
                'verbose_name_plural': 'Customer Information',
                'ordering': ('first_name', 'last_name'),
            },
        ),
        migrations.CreateModel(
            name='TestDrive',
            fields=[
                ('id', models.CharField(default=utility.utils.generate_test_drive_id,
                                        max_length=30, primary_key=True, serialize=False, unique=True)),
                ('activity_no', models.CharField(blank=True, max_length=200)),
                ('referral', models.CharField(blank=True, max_length=200)),
                ('dealership', models.CharField(blank=True, max_length=200)),
                ('sales_person', models.CharField(blank=True, max_length=200)),
                ('next_action', models.CharField(blank=True, max_length=200)),
                ('next_action_date', models.DateTimeField(auto_now=True)),
                ('date_out', models.DateTimeField(auto_now=True)),
                ('time_out', models.TimeField()),
                ('date_due', models.DateTimeField(auto_now=True)),
                ('time_due', models.TimeField()),
                ('comment', models.TextField()),
                ('driver_license_front', models.URLField(blank=True)),
                ('driver_license_back', models.URLField(blank=True)),
                ('signature', models.CharField(blank=True, max_length=500)),
                ('date_returned', models.DateTimeField(auto_now=True)),
                ('time_returned', models.TimeField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('customer', models.ForeignKey(
                    on_delete=django.db.models.deletion.CASCADE, to='customer.customerinfo')),
                ('vehicle', models.ForeignKey(
                    on_delete=django.db.models.deletion.CASCADE, to='vehicle.vechicleinfo')),
            ],
            options={
                'verbose_name': 'Customer Address',
                'verbose_name_plural': 'Customer Address',
            },
        ),
        migrations.CreateModel(
            name='CustomerAddress',
            fields=[
                ('address_id', models.CharField(default=utility.utils.generate_address_id,
                                                max_length=30, primary_key=True, serialize=False, unique=True)),
                ('address', models.TextField()),
                ('suburb', models.CharField(blank=True, max_length=200)),
                ('city', models.CharField(blank=True, max_length=200)),
                ('postcode', models.CharField(blank=True, max_length=200)),
                ('country', models.CharField(blank=True, max_length=300)),
                ('postal_address', models.TextField()),
                ('postal_suburb', models.CharField(blank=True, max_length=200)),
                ('postal_city', models.CharField(blank=True, max_length=200)),
                ('postal_postcode', models.CharField(blank=True, max_length=200)),
                ('postal_country', models.CharField(blank=True, max_length=300)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('customer', models.ForeignKey(
                    on_delete=django.db.models.deletion.CASCADE, to='customer.customerinfo')),
            ],
            options={
                'verbose_name': 'Customer Address',
                'verbose_name_plural': 'Customer Address',
            },
        ),
    ]
